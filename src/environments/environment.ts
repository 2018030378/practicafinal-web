// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCnjpTLV7MibNlOFhpNp_Gbd0dw8kz7llA",
    authDomain: "proyectocorte3-5d367.firebaseapp.com",
    databaseURL: "https://proyectocorte3-5d367-default-rtdb.firebaseio.com",
    projectId: "proyectocorte3-5d367",
    storageBucket: "proyectocorte3-5d367.appspot.com",
    messagingSenderId: "120959527812",
    appId: "1:120959527812:web:fb08109300f8a462744bd1",
    measurementId: "G-K4WDN0PQ9L"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
