import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AutoService } from 'src/app/pages/Automovil/auto.service';
import { Auto } from 'src/app/shared/components/models/auto.interface';
import { FileI } from 'src/app/shared/components/models/file.interface';



@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})

export class NewComponent implements OnInit {
  autoItem : Auto;
  autoToSend : Auto;
  private image : any;
  private filePath : any;
  public url: Observable<string>;

  messages = {
    minLength : "Ingresa el minimo de caracteres",
    required : "Este campo es requerido"
  }

  autoForm : FormGroup;
  constructor(private router : Router, 
    private fb : FormBuilder, 
    private autoService : AutoService,
    private toastr: ToastrService,
    private storage : AngularFireStorage) {

    const navigation = this.router.getCurrentNavigation();
    this.autoItem = navigation?.extras?.state?.value;
    this.initForm();
  }

  ngOnInit(): void {

    if(typeof this.autoItem === 'undefined'){
      //redirect
      this.router.navigate(['new']);
    }else {
      this.autoForm.patchValue(this.autoItem);
      this.url = this.autoItem.url;
    }
  }

  private initForm() : void {
    this.autoForm = this.fb.group({
      titulo : ['', [Validators.required, Validators.minLength(5)] ],
      descripcion : ['', [Validators.required,Validators.minLength(5)] ],
      url : ['']
    });
  }

  isValidField (field : string) : string {
    const validatedField = this.autoForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid' : validatedField.touched ? 'is-valid' : '';
  }

  async handleImage(event : any) {
    this.image = event.target.files[0];
    console.log('image', this.image);
    this.uploadImage(this.image);
  }


  onSave() : void {
    if(this.autoForm.valid){
      this.autoToSend = this.autoForm.value;
      this.autoToSend.url = this.url;
      const autoId = this.autoItem?.id || null;
      this.autoService.onSaveAutos(this.autoToSend, autoId);
      this.autoForm.reset();
      this.toastr.success('Se guardo con exito');
      this.onGoBack();
    }
  }

  onGoBack() : void {
    this.router.navigate(['home']);
  }

  
  async uploadImage(image : FileI) {
    this.filePath = `images/${image.name}`;
    const fileRef = this.storage.ref(this.filePath);
    try {
      const task = this.storage.upload(this.filePath, image);
      await task.snapshotChanges()
      .pipe(
        finalize( () => {
          fileRef.getDownloadURL().subscribe( urlImage => {
            this.url = urlImage;
            console.log('URL service', urlImage);
          });
        })
      ).subscribe();
    } catch (error) {
      console.error(error);
    }
  }
}
