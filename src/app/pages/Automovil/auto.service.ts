import { Injectable } from '@angular/core';
import { AngularFirestoreCollection } from '@angular/fire/firestore/collection/collection';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';
import { async, reject, resolve } from 'q';
import { FileI } from 'src/app/shared/components/models/file.interface';
import { AngularFireStorage } from '@angular/fire/storage';
import { promise } from 'protractor';
import { Auto } from 'src/app/shared/components/models/auto.interface';

@Injectable({
  providedIn: 'root'
})
export class AutoService {
  auto : Observable<Auto[]>;
  private filePath : any;

  private autosCollection : AngularFirestoreCollection<Auto>;

  constructor(
    private afs : AngularFirestore,
    private storage : AngularFireStorage
  ) {

    this.autosCollection = afs.collection<Auto>('Autos');
    this.getAutos();
  }

  /*
    Save item
  */
  onSaveAutos(Autos : Auto, idAutos : string): Promise<void>{
    return new Promise(async(resolve, reject) => {
      try{
        const id = idAutos || this.afs.createId();
        const data = { id, ...Autos};
        console.log(data);
        const result = this.autosCollection.doc(id).set(data)
        resolve(result);

      }catch(err){
        reject(err.messsage);
      }
    });
  }
  /*
    Get items
  */
  private getAutos(): void{
    this.auto = this.autosCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => a.payload.doc.data() as Auto))
    );
  }
}
