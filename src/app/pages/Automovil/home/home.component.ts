import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { AutoService } from '../auto.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  image = "https://firebasestorage.googleapis.com/v0/b/proyectocorte3-5d367.appspot.com/o/home%2Fsaluddeporte.png?alt=media&token=65901d45-23d5-47cc-ba5a-d321f1cf975a";
  public user$: Observable<any> = this.authService.afAuth.user;

  constructor(private router : Router, 
    private autoService : AutoService,
    private toastr: ToastrService,
    private authService : AuthService) { }

  ngOnInit(): void {
  }

  OnGoOn() : void {
  this.router.navigate(['new']);
  }
  async onLogout(){
    try {
      await this.authService.logout();
      this.router.navigate(['login']);
    } catch (error) {
      console.log(error);
    }
  }
}
