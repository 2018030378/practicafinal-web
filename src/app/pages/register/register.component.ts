import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }
  
  registerForm = new FormGroup ({
    email : new FormControl('', [Validators.required, Validators.email]),
    password : new FormControl('', Validators.required),
    matchingPassword : new FormControl('', Validators.required)
  });

  ngOnInit(): void {}
  
    isValidField (field : string) : string {
    const validatedField = this.registerForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid' : validatedField.touched ? 'is-valid' : '';
  }

  async onSave(){
    const {email, password} = this.registerForm.value;
    try {
      const user = this.authService.register(email, password);
      if(user){
        this.router.navigate(['\home']);
      }
    } catch (error) {
      console.log(error)
    } 
  }
}
