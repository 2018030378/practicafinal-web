import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path :'', redirectTo : '/login', pathMatch : 'full'}, 
  { path: 'new', loadChildren: () => import('./pages/Automovil/new/new.module').then(m => m.NewModule) },
  { path: 'home', loadChildren: () => import('./pages/Automovil/home/home.module').then(m => m.HomeModule) },
  { path: 'register', loadChildren: () => import('./pages/register/register.module').then(m => m.RegisterModule) },
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule) },
  { path: 'singup', loadChildren: () => import('./pages/singup/singup.module').then(m => m.SingupModule) },
  {path: '**', redirectTo: 'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
