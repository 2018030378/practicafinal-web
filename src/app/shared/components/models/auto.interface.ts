import { Observable } from "rxjs";

export interface Auto {
    id? : string;
    titulo : string;
    descripcion : string;
    url? : Observable<string>;
}